import cv2
import os
import sys

"""
this script captures video frames, identifies all present faces, grabs coordinates and dimensions of all present faces,
selects nearest face, and calculates a face position as average of the last N frames.
script prints x coordinate deviation of the frame mean for use in robot rotation.
"""
sys.path.append('/usr/local/lib/python2.7/site-packages')
# behavioural variables
avg_dur = 7  # number of recent frames from which to average face position

# init face detection files
cascPath = os.getcwd() + '/haarcascade_frontalface_default.xml'
faceCascade = cv2.CascadeClassifier(cascPath)

# init webcam cap
video_capture = cv2.VideoCapture(0)

# init of coordinate lists
face_x_coord = []
face_y_coord = []

# grab single webcam frame to get image dimensions
ret_init, init_frame = video_capture.read()

# define middle of screen
frame_mid_x = init_frame.shape[1]/2
#print "x size %d" % frame_mean_x
frame_mid_y = init_frame.shape[0]/2

# detection loop
while True:

    # Capture frame
    ret, frame = video_capture.read()

    # grayscale image created for face detection
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # face detection in current frame
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags = cv2.CASCADE_SCALE_IMAGE
    )

    # check if faces are detected
    if len(faces) > 0:

        # draw rectangle around each detected face
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 5)

        # select first face if several present (beautiful side effect: the first one is always the largest face!)
        focal_face = faces[0]
        #cv2.circle(frame, (focal_face[0], focal_face[1]), 30, (255, 255, 0), 3)
        
        # get mean x and y coordinates of time series
        face_x_coord.append(focal_face[0] + focal_face[2] / 2)
        face_y_coord.append(focal_face[1] + focal_face[3] / 2)

        # average input to compensate for sudden movement and false positive detections, by clipping list of corrdinates to
        # last avg_dur items
        face_x_coord = face_x_coord[-avg_dur:]
        face_y_coord = face_y_coord[-avg_dur:]

        # average each coordinate for 'cushioned' position tracking
        if len(face_x_coord) > 0 and len(face_y_coord) > 0:
            target_x_coord = sum(face_x_coord)/len(face_x_coord)
            target_y_coord = sum(face_y_coord)/len(face_y_coord)

            # draw circle around averaged position
            cv2.circle(frame, (target_x_coord, target_y_coord), 30, (255, 255, 0), 3)

            # for dog_e rotation:
            # output of deviation of x coordinate from the centre of the image
            x_deviation =  target_x_coord - frame_mid_x
            print x_deviation
    else:
        # clear the face coordinates
        face_x_coord = []
        face_y_coord = []
    # Display the resulting frame
    #cv2.imshow('Video', frame)
    key = cv2.waitKey(1)
    if key == 27:  # Esc key to stop
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()